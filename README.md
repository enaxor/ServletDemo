# ServletDemo
A basic blog demonstrating how to use basics servlets with Tomcat8 and Postgresql

1. Install tomcat and mvn
2. Create a postgresql DB with a post table
+---post--+
| id      |
| title   |
| content |
| date    |
+---------+
3. Package with mvn
```mvn package  && install -m 644 target/super-blog.war /var/lib/tomcat8/webapps/super-blog.war```
4. Open http://localhost:8080/super-blog/

![example.png](preview/example.png)
