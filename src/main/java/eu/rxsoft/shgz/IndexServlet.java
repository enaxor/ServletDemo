/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.util.ArrayList;

/**
 * Created by @xarone on 5/4/17.
 * Index servlet
 */
@WebServlet(
        name = "IndexServlet",
        description = "Index page servlet",
        urlPatterns = "" // means '/'
)
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
          // retrive session to welcome users
          String user = null;
          HttpSession session = request.getSession(false);
          if (session != null) {
            user = (String)session.getAttribute("sessionCurrentUser");
            if (user != null) {
                request.setAttribute("Welcome", "Welcome " + user);
            }
          }
          if (user == null) {
            request.setAttribute("Welcome", "<a href='login.jsp'><h2>Log in</h2></a>");
          }
          // get posts
          ArrayList<BlogPost> blogPosts = DBHandler.GetBlogPosts();
          // format posts
          String ret = "";
          for (int i = 0; i < blogPosts.size(); i++) {
              BlogPost value = blogPosts.get(i);
              ret = ret + "<a href='post?id=" + value.getId() + "'><h3>" + value.getTitle() + "</h3></a><p>" + value.getContent() + "</p><pre>" + value.getDate() + "</pre><br>";
          }
          request.setAttribute("BlogPosts", ret);
          // send page
          request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
