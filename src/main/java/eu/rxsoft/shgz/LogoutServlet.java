/*
* Copyright (C) 2016 @xarone
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
* Created by @xarone on 5/4/17.
* Index servlet
*/
@WebServlet(
name = "LogoutServlet",
description = "Logout page servlet",
urlPatterns = "/logout"
)
public class LogoutServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    HttpSession session = request. getSession(false);
    session.invalidate(); // invalid session
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    // include header.jsp file
    request.getRequestDispatcher("header.jsp").include(request, response);
    out.println("<h1>You have been disconnected.</h1><a href='/super-blog'><h2>Return to home page</h2></a>");
    // include footer.jsp file
    request.getRequestDispatcher("footer.jsp").include(request, response);
    out.close();
  }
}
