package eu.rxsoft.shgz;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

public class DataSource {
  private final String dbName = "jdbc:postgresql://localhost/superblog";
  private final String dbDriver = "org.postgresql.Driver";
  private final String userName = "dbuser";
  private final String password = "secret";

    private static DataSource     datasource;
    private BasicDataSource ds;

    private DataSource() throws IOException, SQLException, PropertyVetoException {
        ds = new BasicDataSource();
        ds.setDriverClassName(dbDriver);
        ds.setUsername(userName);
        ds.setPassword(password);
        ds.setUrl(dbName);
    }

    public static DataSource getInstance() throws IOException, SQLException, PropertyVetoException {
        if (datasource == null) {
            datasource = new DataSource();
            return datasource;
        } else {
            return datasource;
        }
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }
}
