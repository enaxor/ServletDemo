/*
* Copyright (C) 2016 @xarone
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.util.ArrayList;

/**
* Created by @xarone on 5/4/17.
* Admin servlet
*/
@WebServlet(
name = "AdminPanelServlet",
description = "Admin Panel",
urlPatterns = "/adminPanel"
)
public class AdminPanelServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    // check if session is active
    HttpSession session = request.getSession(false);
    if (session != null) {
      // session retrieved, continue with servlet operations
      String user = (String)session.getAttribute("sessionCurrentUser");
      if (user == null) {
        response.sendRedirect("403.jsp"); // user not connected
      } else {
        request.setAttribute("Welcome", "Hi " + user);
        request.getRequestDispatcher("adminPanel.jsp").forward(request, response);
      }
    } else {
      // no session, return on login page
      response.sendRedirect("login.jsp");
    }
  }
}
