/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 *
 */
@WebServlet("/singin") // This is the URL of the servlet.
public class SignInServlet extends HttpServlet {
@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String user = request.getParameter("user");
        String password = request.getParameter("password");
				// check authentification
        if (DBHandler.SignInValidation(user, password) == true) {
            // create session
            HttpSession session = request.getSession(true);
            if (session != null) {
              // set session variables
              session.setAttribute("sessionCurrentUser", user);
              response.sendRedirect("adminPanel");
            } else {
              // no session, return an error page
              response.sendRedirect("invalidLogin.jsp");
            }

        } else {
            out.print("Sorry username or password error");
            response.sendRedirect("invalidLogin.jsp");
        }
        out.close();
    }

		public void init(ServletConfig config) throws ServletException {
        super.init(config);
				// test logger
        Logger logger = Logger.getLogger("super-blog-logger");
        logger.info("Init SignInServlet");
    }
}
