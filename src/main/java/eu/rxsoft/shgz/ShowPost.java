/*
* Copyright (C) 2016 @xarone
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import java.util.logging.Logger;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.util.ArrayList;

/**
* Created by @xarone on 5/4/17.
* Post page Servlet
*/
@WebServlet(
name = "ShowBlogPostServlet",
description = "Blog Post page servlet",
urlPatterns = "/post"
)
public class ShowPost extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    String ret;
    String id = (String)request.getParameter("id");
    if (id == null) {
      ret = "<h1>Not found</h1>";
    } else {
      BlogPost blogPost =  DBHandler.GetBlogPost(Integer.parseInt(id));
      ret = "<h1>" + blogPost.getTitle() + "</h1><p>" + blogPost.getContent() + "</p><pre>" + blogPost.getDate() + "</pre>";
    }
    request.setAttribute("post", ret);
    // send page
    request.getRequestDispatcher("post.jsp").forward(request, response);
  }
}
