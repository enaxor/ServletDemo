/*
 * Copyright (C) 2016 @xarone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.util.Date;

/**
 * Created by @xarone on 5/4/17.
 * Simple blog post container
 */
public class BlogPost {
   private final int id;
   private final String title;
   private final String content;
   private final Date date;

   public BlogPost(int id, String title, String content, Date date) {
     this.id = id;
     this.title = title;
     this.content = content;
     this.date = date;
   }
   public int getId() {
     return this.id;
   }
   public String getTitle() {
     return this.title;
   }
   public String getContent() {
     return this.content;
   }
   public Date getDate() {
     return this.date;
   }

   public String toString() {
     return "Title:" + title + "content" + content + "date" + date.toString();
   }
}
