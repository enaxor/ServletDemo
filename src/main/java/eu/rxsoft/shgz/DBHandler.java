/*
* Copyright (C) 2016 @xarone
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package eu.rxsoft.shgz;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.sql.*;
import java.beans.PropertyVetoException;

/**
* Created by @xarone on 5/4/17.
* Database controller
*/
public class DBHandler {
  private final static String USER_ID = "admin";
  private final static String USER_PWD = "admin";

  private static ArrayList<BlogPost> blogPosts = new ArrayList<BlogPost>();
  private Logger logger;

  // populate blog post
  private void populatePosts() {
    ArrayList<BlogPost> posts = new ArrayList<BlogPost>();
    String sql = "SELECT id, title, content, date FROM post;";
    try {
      Connection connection = DataSource.getInstance().getConnection();
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery(sql);
      while (rs.next()) {
        posts.add(new BlogPost(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getDate("date")));
      }
      // close statement
      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
  }

  public static boolean SignInValidation(String name,String pass) {
    return name.equals(USER_ID) && pass.equals(USER_PWD); // TODO add users in DB
  }

  public static ArrayList<BlogPost> GetBlogPosts() {
    ArrayList<BlogPost> blogPosts = new ArrayList<BlogPost>();
    String sql = "SELECT id, title, content, date FROM post;";
    try {
      Connection connection = DataSource.getInstance().getConnection();
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery(sql);
      while (rs.next()) {
        blogPosts.add(new BlogPost(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getDate("date")));
      }
      // close statement
      statement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    return blogPosts;
  }

  public static BlogPost GetBlogPost(int id) {
    BlogPost post = null;
    String sql = "SELECT id, title, content, date FROM post WHERE id = " + id + " LIMIT 1;";
    try {
      Connection connection = DataSource.getInstance().getConnection();
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery(sql);
      while (rs.next()) {
        post = new BlogPost(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getDate("date"));
      }
      statement.close();
    } catch(SQLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    return post;
  }

  public static void AddBlogPost(String title, String content) {
    String query = "INSERT INTO post (title, content, date) VALUES (?, ?, TIMESTAMP '2017-04-1 14:25:38')"; // TODO format timestamp from Date
    try {
      Connection connection = DataSource.getInstance().getConnection();
      // set all the preparedstatement parameters
      PreparedStatement preparedstatement = connection.prepareStatement(query);
      preparedstatement.setString(2, title);
      preparedstatement.setString(3, content);
      // execute preparedstatement
      preparedstatement.executeUpdate();
      preparedstatement.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
  }
}
