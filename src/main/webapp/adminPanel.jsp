<%@include  file="header.jsp" %>
<h2><%= request.getAttribute("Welcome") %></h2>
<h2>You can modify blog posts here.</h2>
<h2>
    <a href='logout'>Log out</a>
</h2>
<h6>Add</h6>
<form action='addBlogPost' method='post'>
    <input type='text' name='title' value='' placeholder='Title' required='true'/>
    <br/>
    <textarea rows='4' cols='50' name='content' required='true' placeholder='Write the content of the blog post here'></textarea>
    <br/>
    <input type='submit' value='Add New Post'/>
</form>
<%@include  file="footer.jsp" %>
